const rock_user = document.getElementById("r-user");
const paper_user = document.getElementById("p-user");
const scissors_user = document.getElementById("s-user");
const rock_com = document.getElementById("r-com");
const paper_com = document.getElementById("p-com");
const scissors_com = document.getElementById("s-com");
const vsBox = document.getElementById("vs");
const result = document.getElementById("h1");
const refresh = document.getElementById("refresh");
const clicked = document.getElementById("human");
const matches = document.querySelectorAll("button.user-hand, button.com-hand")

function comBrain() {
  const choices = ["Rock", "Paper", "Scissors"];
  const random = Math.floor(Math.random() *3);
  return choices [random];
}

function resultObject() {
  vsBox.classList.add("resultBox");
  result.setAttribute("style", "font-size:38px; color:white;");
}

function win() {
  console.log("Player 1 Win");
  resultObject();
  result.innerText = "Player 1 \nWIN";

}

function lose() {
  console.log("COM WIN");
  resultObject();
  result.innerText = "COM \nWIN";
}

function draw() {
  console.log("Draw");
  resultObject();
  result.innerText = "Draw";
}

function game(userChoice) {
  const comUser = comBrain();
  console.log("Player 1 = " + userChoice);
  console.log("Com = " + comUser);

  switch (userChoice + comUser) {
    case "PaperRock":
    case "RockScissors":
    case "ScissorsPaper":
      win();

    break;
    case "RockPaper":
    case "ScissorsRock":
    case "PaperScissors":
      lose();
    
    break;
    case "RockRock":
    case "ScissorsScissors":
    case "PaperPaper":
      draw();
  }

  switch (comUser) {
    case "Rock":
      rock_com.classList.add("chosen");

      break;
    case "Paper":
      paper_com.classList.add("chosen");

      break;
    case "Scissors":
      scissors_com.classList.add("chosen");
  }
}

class Handplay {
  constructor(id, choice) {
    this.id = id;
    this.choice = choice;
  }

  play() {
    this.id.classList.add("chosen");
    game(this.choice);
    clicked.setAttribute("style", "cursor: not-allowed; pointer-events: none;");
  }
}

const Rock = new Handplay(rock_user, "Rock");
const Paper = new Handplay(paper_user, "Paper");
const Scissors = new Handplay(scissors_user, "Scissors");

function reload() {
  matches.forEach(element => {
    element.classList.remove("chosen")})
  clicked.removeAttribute("style", "cursor: not-allowed; pointer-events: none;")
  vsBox.classList.remove("resultBox")
  result.removeAttribute("style", "color: ''; font-size:'' ")
  result.innerText = "VS"
}